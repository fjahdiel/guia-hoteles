$(function () {
    $("[data-toggle='tooltip']").tooltip();
    $("[data-toggle='popover']").popover();
    $('.carousel').carousel({ 
        interval: 2000
    });

    /* Accion cuando se esta mostrando el modal*/
    $('#ContactoModalCenter').on('show.bs.modal',function(e){
        console.log('El modal se esta mostrando');
        console.log('Se inhabilita el boton y cambia de color');
        $('#contactobtn').disabled = true;
        $('#contactobtn').css('background-color','#f5ec42');
    });
    /* Accion cuando se termina de mostrar el modal*/
    $('#ContactoModalCenter').on('shown.bs.modal',function(e){
        console.log('El modal se mostro por completo');
    });
     /* Accion cuando se empieza a ocultar el modal*/
     $('#ContactoModalCenter').on('hidde.bs.modal',function(e){
        console.log('El modal se esta ocultando');
    });
    /* Accion cuando se oculta el modal*/
    $('#ContactoModalCenter').on('hidden.bs.modal',function(e){
        console.log('El modal se oculto por completo');
        console.log('Se habilita el boton y cambia de color');
        $('#contactobtn').disabled = false;
        $('#contactobtn').css('background-color','#fff');
    });
});